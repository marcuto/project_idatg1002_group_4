# Football Tournament Administration
#### Group 4
 Group members: <br />
 Jostein Lind <br />
 Marcus Torvik <br />
 Håvard Moen Lajord <br />
 Sigrid Warberg Vestheim <br />
 Thomas Gjerde <br />

#### The project
This is a project in IDATG1002 where we implemented a system for a football tournament. The main user is the administrator of the tournament. The system has these functions
* Create a new tournament
* Add teams
* Add the results from the matches
* Generate a table with points
* Generate knockout of the teams
* Continue a tournament 

#### The installation guide
See the link to know how to install the program <br />
https://gitlab.stud.idi.ntnu.no/marcuto/project_idatg1002_group_4/-/wikis/installation%20manual

