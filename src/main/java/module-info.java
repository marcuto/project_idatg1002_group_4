module GUI {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires java.sql;

    opens GUI to javafx.fxml;
    exports GUI;

    opens no.idatg1002.TournamentApp;
    exports no.idatg1002.TournamentApp;
}
