package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * This is the class that runs the application
 */
public class TournamentApplication extends Application {

    /**
     * Method that loads the second page(TournamentNameController).
     * @param stage The window that will be displayed.
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(TournamentApplication.class.getResource("TournamentMainMenuController.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 900, 700);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/FTA_icon.png")));
        stage.setTitle("Football Tournament");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Standard psvm method that launches the application.
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }
}

