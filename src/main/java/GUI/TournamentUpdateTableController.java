package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import no.idatg1002.TournamentApp.*;
import javafx.scene.control.Alert.AlertType;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Controller class for page five. This class will provide the functionality for page five fxml file.
 */
public class TournamentUpdateTableController implements Initializable{
    @FXML
    private Label labelOfTournamentName;
    @FXML
    private ObservableList<Team> observableListOfTableElements;
    @FXML
    public TableView<Team> fullTableDisplay;
    @FXML
    public TableColumn<Team, String> teamName;
    @FXML
    public TableColumn<Team, Integer> gp;
    @FXML
    public TableColumn<Team, Integer> w;
    @FXML
    public TableColumn<Team, Integer> d;
    @FXML
    public TableColumn<Team, Integer> l;
    @FXML
    public TableColumn<Team, Integer> gd;
    @FXML
    public TableColumn<Team, Integer> p;
    @FXML
    public TableColumn<Team, Integer> position;
    @FXML
    private TextField resultGoalTeamOne;
    @FXML
    private TextField resultGoalTeamTwo;
    @FXML
    private TextField resultNameTeamOne;
    @FXML
    private TextField resultNameTeamTwo;

    private TournamentTable tableTournament;

    private int matchIdx;

    private TournamentFileHandler tournamentFileHandler;


    @FXML
    private Button backToMainMenuButtonFour;

    /**
     * Button handler for "Back to main menu" button. This will load page one.
     * @throws IOException if an IO exception has occurred.
     */
    public void handleBackToMainMenuButtonFour() throws IOException {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Back to main menu");
        alert.setHeaderText("You're about to return to main menu and lose new progress.");
        alert.setContentText("Is this what you want?");
        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentMainMenuController.fxml"));
            Stage stage = (Stage) backToMainMenuButtonFour.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
        }
    }

    /**
     * Button handler for "Next Match" button. This method will translate the result of a match between two teams
     * to the table.
     */
    public void handleNextMatchButton(){
        if(resultGoalTeamOne.getText().matches("^\\d+$") && resultGoalTeamTwo.getText().matches("^\\d+$")){
            if (matchIdx < this.tableTournament.getMatches().size()) {
                Match currentMatch = this.tableTournament.getMatches().get(matchIdx);
                currentMatch.setScore(resultGoalTeamOne.getText()+":"+resultGoalTeamTwo.getText());
                currentMatch.setWinner();
                currentMatch.updateValues();
                matchIdx++;
                setResultNames();
                this.tableTournament.updateRankings();
                observableListOfTableElements.set(0, observableListOfTableElements.get(0));
            }
            else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("No more games!");
                alert.setHeaderText("There are no more games in this tournament.");
                alert.show();
            }
            resultGoalTeamTwo.clear();
            resultGoalTeamOne.clear();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong input!");
            alert.setHeaderText("Integer expected.");
            alert.show();
            resultGoalTeamTwo.clear();
            resultGoalTeamOne.clear();
        }

    }

    /**
     * Method that gives the user an option to use the ENTER key, rather than clicking the button.
     * @param ke Key to be pressed.
     */
    public void onEnter(KeyEvent ke) {
        if (ke.getCode().equals(KeyCode.ENTER)) {
            handleNextMatchButton();
        }
    }


    /**
     * Button handler for "End Tournament" button. Will ask if user wants to end tournament, and if the answer is yes,
     * load page one and save the current tournament.
     * @throws IOException if an IO exception has occurred.
     */
    public void handleEndTournamentButton () throws IOException {
        tournamentFileHandler = new TournamentFileHandler();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("End Tournament");
        alert.setHeaderText("You're about to end this tournament and return to main menu. You can still continue current tournament");
        alert.setContentText("Is this what you want?");

        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentMainMenuController.fxml"));
            Stage stage = (Stage) backToMainMenuButtonFour.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
            tournamentFileHandler.writeTournamentMatchCsv(this.tableTournament.getMatches(), this.tableTournament.getNameOfTournament(), this.tableTournament.getClass().getSimpleName());
            tournamentFileHandler.writeTeamsCsv(this.tableTournament.getTeamsInTournament());
        }
    }
    

    /**
     * Initializes the contents of the list of teams, and allows the table to be updated live.
     * @param url
     * @param resourceBundle
     */
     @Override
     public void initialize (URL url, ResourceBundle resourceBundle){
         matchIdx = 0;
         this.tableTournament = new TournamentTable();
        observableListOfTableElements = FXCollections.observableList(tableTournament.getTeamsInTournament());
        fullTableDisplay.setItems(observableListOfTableElements);
        position.setCellValueFactory(new PropertyValueFactory<>("position"));
        teamName.setCellValueFactory(new PropertyValueFactory<>("teamName"));
        gp.setCellValueFactory(new PropertyValueFactory<>("gamesPlayed"));
        w.setCellValueFactory(new PropertyValueFactory<>("wins"));
        d.setCellValueFactory(new PropertyValueFactory<>("draws"));
        l.setCellValueFactory(new PropertyValueFactory<>("losses"));
        gd.setCellValueFactory(new PropertyValueFactory<>("goalDifference"));
        p.setCellValueFactory(new PropertyValueFactory<>("points"));
    }


    /**
     * Transfers the name of the tournament to this page for display.
     * @param tournamentName Name of the tournament, entered on page two.
     */
    public void setNameOfTournament (String tournamentName) {
        labelOfTournamentName.setText(tournamentName);
    }

    public void setTournament(Tournament tournament){
        this.tableTournament = (TournamentTable) tournament;
        observableListOfTableElements = FXCollections.observableList(tableTournament.getTeamsInTournament());
        fullTableDisplay.setItems(observableListOfTableElements);
        labelOfTournamentName.setText(tournament.getNameOfTournament());
        this.matchIdx = (int) this.tableTournament.getMatches().stream().filter(m -> m.getWinner() != null).count();
        setResultNames();
        position.setCellValueFactory(new PropertyValueFactory<>("position"));
        teamName.setCellValueFactory(new PropertyValueFactory<>("teamName"));
        gp.setCellValueFactory(new PropertyValueFactory<>("gamesPlayed"));
        w.setCellValueFactory(new PropertyValueFactory<>("wins"));
        d.setCellValueFactory(new PropertyValueFactory<>("draws"));
        l.setCellValueFactory(new PropertyValueFactory<>("losses"));
        gd.setCellValueFactory(new PropertyValueFactory<>("goalDifference"));
        p.setCellValueFactory(new PropertyValueFactory<>("points"));
    }
    public void setListOfTeams(List<Team> teams){this.tableTournament.getTeamsInTournament().addAll(teams);
        observableListOfTableElements.set(0, observableListOfTableElements.get(0));
        this.tableTournament.createMatches();
        setResultNames();
    }

    private void setResultNames(){
        if (matchIdx < this.tableTournament.getMatches().size()) {
            Match currentMatch = this.tableTournament.getMatches().get(matchIdx);
            this.resultNameTeamOne.setText(currentMatch.getTeamOne().getTeamName());
            this.resultNameTeamTwo.setText(currentMatch.getTeamTwo().getTeamName());
        }
    }


}

