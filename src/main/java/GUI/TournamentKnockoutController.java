package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import no.idatg1002.TournamentApp.*;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller class for page six. This class will provide the functionality for page six fxml file.
 */
public class TournamentKnockoutController implements Initializable {
    @FXML
    private Label labelOfTournamentName;
    @FXML
    private TableView<Match> knockoutTableView;
    @FXML
    private TableColumn<Match, String> matchDisplay;
    @FXML
    private TextField fieldTeamOne;
    @FXML
    private TextField fieldTeamTwo;
    @FXML
    private TextField goalsTeamOne;
    @FXML
    private TextField goalsTeamTwo;
    @FXML
    private Button buttonEndTournament;
    @FXML
    private Button buttonBackToMain;

    private TournamentKnockout knockoutTournament;
    private int matchIdx;
    private TournamentFileHandler tournamentFileHandler;
    private ObservableList<Match> observableListOfTableElements;

    /**
     * Method that responds to the "Next Round" button and starts the next round.
     */
    @FXML
    public void handleButtonNextRound(){

        if(matchIdx < knockoutTournament.getMatches().size()){
            makeAlert(Alert.AlertType.ERROR, "Still More Matches!", "You need to add the results for " +
                    "all the matches before proceeding").show();
        }
        else if(matchIdx == knockoutTournament.getMatches().size() && knockoutTournament.getWinners().size() == 1){
            makeAlert(Alert.AlertType.ERROR, "End of Tournament", "Tournament has ended, and therefore " +
                    "has no more matches").show();
        }
        else if(matchIdx == knockoutTournament.getMatches().size()){
            knockoutTournament.nextRound();
            matchIdx = 0;
            setResultNames();
            observableListOfTableElements.set(0, observableListOfTableElements.get(0));
        }
    }

    /**
     * Method that responds to the "End Tournament" button, saves the current tournament and
     * loads page one(TournamentApplication page).
     * @throws IOException if an IO exception has occurred.
     */
    @FXML
    public void handleButtonEndTournament() throws IOException {
        tournamentFileHandler = new TournamentFileHandler();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("End Tournament");
        alert.setHeaderText("You're about to end this tournament and return to main menu. You can still continue on current tournament");
        alert.setContentText("Is this what you want?");

        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentMainMenuController.fxml"));
            Stage stage = (Stage) buttonEndTournament.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
            tournamentFileHandler.writeTournamentMatchCsv(this.knockoutTournament.getMatches(), this.knockoutTournament.getNameOfTournament(), this.knockoutTournament.getClass().getSimpleName());
            tournamentFileHandler.writeTeamsCsv(this.knockoutTournament.getTeamsInTournament());
        }
    }

    /**
     * Method that responds to the "Back To Main Menu" button and loads page one(TournamentMainMenu page.)
     * @throws IOException if an IO exception has occurred.
     */
    @FXML
    public void handleButtonBackToMain() throws IOException{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Back to main menu");
        alert.setHeaderText("You're about to return to main menu and lose new progress.");
        alert.setContentText("Is this what you want?");
        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentMainMenuController.fxml"));
            Stage stage = (Stage) buttonBackToMain.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
        }
    }

    /**
     * Method that responds to the "Add" button and adds the result of the match to the tournament.
     */
    @FXML
    public void handleAddResultButton(){
        if(goalsTeamOne.getText().isBlank() || goalsTeamTwo.getText().isBlank()){
            makeAlert(Alert.AlertType.ERROR, "Blank input!", "You need to enter the score in the fields first.").show();
        }
        else if (matchIdx < this.knockoutTournament.getMatches().size()) {
            Match currentMatch = this.knockoutTournament.getMatches().get(matchIdx);
            while (currentMatch.getWinner() != null && matchIdx+1 < this.knockoutTournament.getMatches().size()){
                matchIdx++;
                currentMatch = this.knockoutTournament.getMatches().get(matchIdx);
            }
            if(goalsTeamOne.getText().matches("^\\d+$") && goalsTeamTwo.getText().matches("^\\d+$")){
                if(goalsTeamOne.getText().equals(goalsTeamTwo.getText())){
                    makeAlert(Alert.AlertType.ERROR, "Wrong Input", "As this is a knockout tournament, you cannot" +
                            " have a draw. If the game went to penalties, include the penalty goals in the final score").show();
                    return;
                }
                else{
                    currentMatch.setScore(goalsTeamOne.getText()+":"+goalsTeamTwo.getText());
                }
            }
            else{
                makeAlert(Alert.AlertType.ERROR, "Wrong Input", "You need to input a number").show();
                goalsTeamOne.clear();
                goalsTeamTwo.clear();
                return;
            }
            goalsTeamOne.clear();
            goalsTeamTwo.clear();
            currentMatch.setWinner();
            matchIdx++;
            setResultNames();
            observableListOfTableElements.set(0, observableListOfTableElements.get(0));
        }
        else{
            makeAlert(Alert.AlertType.ERROR, "No more matches!", "There are no more matches in this round").show();
            goalsTeamOne.clear();
            goalsTeamTwo.clear();
        }
    }

    /**
     * Method that gives the user the option to use the ENTER key, rather than having to click the buttons.
     * @param ke The key to be pressed.
     */
    public void onEnter(KeyEvent ke) {
        if (ke.getCode().equals(KeyCode.ENTER)) {
            handleAddResultButton();
        }
    }

    /**
     * Initialize method to initialize values and display results live.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        matchIdx = 0;
        this.knockoutTournament = new TournamentKnockout();
        observableListOfTableElements = FXCollections.observableList(knockoutTournament.getMatches());
        knockoutTableView.setItems(observableListOfTableElements);
        matchDisplay.setCellValueFactory(new PropertyValueFactory<>("matchDisplay"));
    }

    /**
     * Transfers the name of the tournament to this page for display.
     * @param tournamentName Name of the tournament, entered on page two.
     */
    public void setNameOfTournament(String tournamentName){
        labelOfTournamentName.setText(tournamentName);
    }

    /**
     * Method that adds a list of teams to the knockout tournament.
     * @param teams The list to be added.
     */
    public void setListOfTeams(List<Team> teams){
        this.knockoutTournament.getTeamsInTournament().addAll(teams);
        knockoutTournament.createMatches();

        setResultNames();
        observableListOfTableElements.set(0, observableListOfTableElements.get(0));
    }

    /**
     * Method that displays the name of the teams in the current match.
     */
    private void setResultNames(){
        if (matchIdx < this.knockoutTournament.getMatches().size()) {
            Match currentMatch = this.knockoutTournament.getMatches().get(matchIdx);
            while (currentMatch.getWinner() != null && matchIdx+1 < this.knockoutTournament.getMatches().size()){
                matchIdx++;
                currentMatch = this.knockoutTournament.getMatches().get(matchIdx);
            }
            this.fieldTeamOne.setText(currentMatch.getTeamOne().getTeamName());
            this.fieldTeamTwo.setText(currentMatch.getTeamTwo().getTeamName());
        }
    }

    /**
     * Method that creates a knockout tournament.
     * @param tournament Tournament to be created.
     */
    public void setTournament(Tournament tournament){
        this.knockoutTournament = (TournamentKnockout) tournament;
        observableListOfTableElements = FXCollections.observableList(knockoutTournament.getMatches());
        knockoutTableView.setItems(observableListOfTableElements);
        matchDisplay.setCellValueFactory(new PropertyValueFactory<>("matchDisplay"));
        this.matchIdx = (int) this.knockoutTournament.getMatches().stream().filter(m -> m.getWinner() != null).count();
        setResultNames();
    }

    /**
     * Method to create alerts.
     * @param alertType Type of alert.
     * @param title Title of the alert.
     * @param headerText The header text of the alert.
     * @return Returns alert with values.
     */
    public Alert makeAlert(Alert.AlertType alertType, String title, String headerText){
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        return alert;
    }
}
