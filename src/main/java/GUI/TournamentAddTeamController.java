package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import no.idatg1002.TournamentApp.Team;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller class for page three. This class will provide the functionality for page three fxml file.
 */
public class TournamentAddTeamController implements Initializable {
    @FXML
    private List<Team> listOfTeams;
    @FXML
    private ObservableList<Team> observableListOfTeams;
    @FXML
    private Button generateTableButton;
    @FXML
    private Button generateKnockoutButton;
    @FXML
    private TableView<Team> teamNameTable;
    @FXML
    private TableColumn<Team, String> teamNameColumn;
    @FXML
    private TextField enterTeamNameField;
    @FXML
    private Button backToMainMenuButtonTwo;

    private String tournamentNameForPageThree;
    private int posIdx;

    /**
     * Button handler for "Back to main menu" button. This will load page one (TournamentMainMenu page).
     * @throws IOException if an IO exception has occurred.
     */
    public void handleBackToMainMenuButtonTwo() throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Back to main menu");
        alert.setHeaderText("You're about to return to main menu and lose new progress.");
        alert.setContentText("Is this what you want?");
        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentMainMenuController.fxml"));
            Stage stage = (Stage) backToMainMenuButtonTwo.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
        }
    }

    /**
     * This method will load page four and provide the parameters needed for the methods in TournamentDisplayTableController.
     * @throws IOException if an IO exception has occurred.
     */
    public void handleGenerateTableButton() throws IOException {
        if(observableListOfTeams.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("No teams!");
            alert.setHeaderText("You have not provided any teams for the table.");
            alert.show();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Warning!");
            alert.setHeaderText("You cannot change the teams in the tournament beyond this point.");
            alert.setContentText("Are you sure you want to continue?");
            if(alert.showAndWait().get() == ButtonType.OK){
                FXMLLoader loader = new FXMLLoader(getClass().getResource("TournamentDisplayTableController.fxml"));
                Parent root = loader.load();
                Stage stage = (Stage) generateTableButton.getScene().getWindow();
                stage.setScene(new Scene(root, 900, 700));

                TournamentDisplayTableController controller = loader.getController();
                controller.setListOfTeams(listOfTeams);
                controller.setNameOfTournament(tournamentNameForPageThree);
            }
        }
    }

    /**
     * This method responds to the "Generate Knockout" button and will load page six (TournamentKnockout page).
     * @throws IOException if an IO exception has occurred.
     */
    public void handleGenerateKnockoutButton() throws IOException {
        if(observableListOfTeams.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("No teams!");
            alert.setHeaderText("You have not provided any teams for the knockout tournament");
            alert.show();
        }
        else if(observableListOfTeams.size() == 1 || !isPowerOfTwo(observableListOfTeams.size())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong amount of teams!");
            alert.setHeaderText("Teams in knockout format need to be to the power of two. For example 2, 4 or 8");
            alert.show();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Warning!");
            alert.setHeaderText("You cannot change the teams in the tournament beyond this point.");
            alert.setContentText("Are you sure you want to continue?");
            if(alert.showAndWait().get() == ButtonType.OK){
                FXMLLoader loader = new FXMLLoader(getClass().getResource("TournamentKnockoutController.fxml"));
                Parent root = loader.load();
                Stage stage = (Stage) generateKnockoutButton.getScene().getWindow();
                stage.setScene(new Scene(root, 900, 700));

                TournamentKnockoutController controller = loader.getController();
                controller.setListOfTeams(listOfTeams);
                controller.setNameOfTournament(tournamentNameForPageThree);
            }
        }
    }

    /**
     * This method responds to the "Add" button and will add a team to the table and update it live.
     * @throws IOException if an IO exception has occurred.
     */
    public void addTeamToTable(){
        if(enterTeamNameField.getText().isBlank()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Team name is empty!");
            alert.setHeaderText("You have to write a name for the team.");
            alert.show();
        }
        else{
            listOfTeams.add(new Team(enterTeamNameField.getText()));
            listOfTeams.get(posIdx).setPosition(posIdx+1);
            posIdx++;
            observableListOfTeams.set(0, observableListOfTeams.get(0));
            enterTeamNameField.clear();
        }
    }

    /**
     * Method that gives the user the option to use the ENTER key, rather than clicking the button.
     * @param ke The key that is pressed.
     */
    public void onEnter(KeyEvent ke){
        if (ke.getCode().equals(KeyCode.ENTER)) {
            addTeamToTable();
        }
    }

    /**
     * Method that responds to the "Delete" button and lets the user delete a team from the tableView.
     */
    public void deleteTeamFromTable(){
        teamNameTable.getItems().removeAll(
                teamNameTable.getSelectionModel().getSelectedItems()
        );
        posIdx = posIdx - 1;
    }


    /**
     * Initializes the lists, which allows live updates of the table.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        posIdx = 0;
        listOfTeams = new ArrayList<>();
        observableListOfTeams = FXCollections.observableList(listOfTeams);
        teamNameTable.setItems(observableListOfTeams);
        teamNameColumn.setCellValueFactory(new PropertyValueFactory<>("teamName"));
    }

    /**
     * Transfers the name of the tournament to this page for display.
     * @param tournamentName Name of the tournament, entered on page two.
     */
    public void setTournamentName(String tournamentName){
        tournamentNameForPageThree = tournamentName;
    }

    /**
     * Checks if a value is power of two.
     * @param n Number to be checked.
     * @return Returns true or false.
     */
    static boolean isPowerOfTwo(int n)
    {
        return (int)(Math.ceil((Math.log(n) / Math.log(2))))
                == (int)(Math.floor((Math.log(n) / Math.log(2))));
    }
}
