package GUI;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import javafx.stage.Stage;
import no.idatg1002.TournamentApp.Tournament;
import no.idatg1002.TournamentApp.TournamentFileHandler;
import no.idatg1002.TournamentApp.TournamentKnockout;
import no.idatg1002.TournamentApp.TournamentTable;

import java.io.IOException;

/**
 * Controller class for page one. This class will provide the functionality for page one fxml file.
 */
public class TournamentMainMenuController {
    @FXML
    private Button button1;
    @FXML
    private Button continueButton;

    private TournamentFileHandler tournamentFileHandler;
    /**
     * This method responds to the "Create New Tournament" button and  will load page two (TournamentName page).
     * @throws IOException if an IO exception has occurred.
     */
    public void handleButtonOne() throws IOException{
        tournamentFileHandler = new TournamentFileHandler();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Create new tournament");
        alert.setHeaderText("This will overwrite any other tournaments and create a blank one");
        alert.setContentText("Is this what you want?");
        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentNameController.fxml"));
            Stage stage = (Stage) button1.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
            tournamentFileHandler.clearAll();
        }
    }
    /**
     * This method responds to the "Continue Tournament" button and will load either page five(TournamentUpdateTable page)
     * or page six (TournamentKnockout page).
     * @throws IOException if an IO exception has occurred.
     */
    public void handleButtonContinue() throws IOException{
        tournamentFileHandler = new TournamentFileHandler();
        Tournament continueTournament = tournamentFileHandler.readTournamentMatchCsv();
        String pageName ="";
        if (continueTournament instanceof TournamentTable){
            pageName = "TournamentUpdateTableController.fxml";
            FXMLLoader loader = new FXMLLoader(getClass().getResource(pageName));
            Parent root = loader.load();
            Stage stage = (Stage) continueButton.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));

            TournamentUpdateTableController controller = loader.getController();
            controller.setTournament(continueTournament);
        } else if (continueTournament instanceof TournamentKnockout){
            pageName = "TournamentKnockoutController.fxml";
            FXMLLoader loader = new FXMLLoader(getClass().getResource(pageName));
            Parent root = loader.load();
            Stage stage = (Stage) continueButton.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));

            TournamentKnockoutController controller = loader.getController();
            controller.setTournament(continueTournament);
        }
    }
}
