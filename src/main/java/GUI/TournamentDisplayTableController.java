package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import no.idatg1002.TournamentApp.Team;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller class for page four. This class will provide the functionality for page four fxml file.
 */
public class TournamentDisplayTableController implements Initializable {
    @FXML
    private Label labelOfTournamentName;
    @FXML
    private Button startTournamentButton;
    @FXML
    public TableView<Team> fullTableDisplay;
    @FXML
    public TableColumn<Team, String> teamName;
    @FXML
    public TableColumn<Team, Integer> gp;
    @FXML
    public TableColumn<Team, Integer> w;
    @FXML
    public TableColumn<Team, Integer> d;
    @FXML
    public TableColumn<Team, Integer> l;
    @FXML
    public TableColumn<Team, Integer> gd;
    @FXML
    public TableColumn<Team, Integer> p;
    @FXML
    public TableColumn<Team, Integer> position;
    @FXML
    private Button backToMainMenuButtonThree;

    private String nameOfTournament;
    private List<Team> listOfTeams;
    private ObservableList<Team> observableListOfTableElements;

    /**
     * Button handler for "Back to main menu" button. This will load page one(TournamentMainMenu page).
     * @throws IOException if an IO exception has occurred.
     */
    public void handleBackToMainMenuButtonThree() throws IOException{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Back to main menu");
        alert.setHeaderText("You're about to return to main menu and lose new progress.");
        alert.setContentText("Is this what you want?");
        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentMainMenuController.fxml"));
            Stage stage = (Stage) backToMainMenuButtonThree.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
        }
    }

    /**
     * This method will laod page five and provide parameters for methods in TournamentUpdateTableController.
     * These parameters will transfer information to the next page.
     * @throws IOException if an IO exception has occurred.
     */
    public void handleStartTournamentButton() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("TournamentUpdateTableController.fxml"));
        Parent root = loader.load();
        Stage stage = (Stage) startTournamentButton.getScene().getWindow();
        stage.setScene(new Scene(root, 900, 700));

        TournamentUpdateTableController controller = loader.getController();
        controller.setNameOfTournament(nameOfTournament);
        controller.setListOfTeams(listOfTeams);
    }

    /**
     * Initializes the contents of the list of teams, and allows the table to be updated live.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        this.listOfTeams = new ArrayList<>();
        observableListOfTableElements = FXCollections.observableList(listOfTeams);
        fullTableDisplay.setItems(observableListOfTableElements);
        position.setCellValueFactory(new PropertyValueFactory<>("position"));
        teamName.setCellValueFactory(new PropertyValueFactory<>("teamName"));
        gp.setCellValueFactory(new PropertyValueFactory<>("gamesPlayed"));
        w.setCellValueFactory(new PropertyValueFactory<>("wins"));
        d.setCellValueFactory(new PropertyValueFactory<>("draws"));
        l.setCellValueFactory(new PropertyValueFactory<>("losses"));
        gd.setCellValueFactory(new PropertyValueFactory<>("goalDifference"));
        p.setCellValueFactory(new PropertyValueFactory<>("points"));
    }

    /**
     * Transfers the name of the tournament to this page for display.
     * @param tournamentName Name of the tournament, entered on page two.
     */
    public void setNameOfTournament(String tournamentName){
        labelOfTournamentName.setText(tournamentName);
        nameOfTournament = tournamentName;
    }

    /**
     * Transfers the list of teams in the tournament to this page for display.
     * @param teams Teams in the tournament, entered on page three.
     */
    public void setListOfTeams(List<Team> teams){
        this.listOfTeams.addAll(teams);
        observableListOfTableElements.set(0, observableListOfTableElements.get(0));
    }
}
