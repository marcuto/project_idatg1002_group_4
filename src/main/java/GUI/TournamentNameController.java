package GUI;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller class for page two. This class will provide the functionality for page two fxml file.
 */
public class TournamentNameController {

    private String tournamentName;
    @FXML
    private Button buttonNextPageTwo;
    @FXML
    private Button backToMainMenuButtonOne;
    @FXML
    private TextField tournamentNameField;

    /**
     * Button handler for "Back to main menu" button. This will load page one.
     * @throws IOException if an IO exception has occurred.
     */
    public void handleBackToMainMenuButtonOne() throws IOException{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Back to main menu");
        alert.setHeaderText("You're about to return to main menu and lose new progress.");
        alert.setContentText("Is this what you want?");
        if(alert.showAndWait().get() == ButtonType.OK){
            Parent root = FXMLLoader.load(getClass().getResource("TournamentMainMenuController.fxml"));
            Stage stage = (Stage) backToMainMenuButtonOne.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));
        }
    }

    /**
     * This method will load page three with the information provided on this page.
     * @throws IOException if an IO exception has occurred.
     */
    public void handleButtonNext() throws IOException{
        if(tournamentNameField.getText().isBlank()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Tournament needs a name");
            alert.show();
        }
        else{
            tournamentName = tournamentNameField.getText();
        }
        if(tournamentName != null){
            FXMLLoader loader = new FXMLLoader(getClass().getResource("TournamentAddTeamController.fxml"));
            Parent root = loader.load();
            Stage stage = (Stage) buttonNextPageTwo.getScene().getWindow();
            stage.setScene(new Scene(root, 900, 700));

            TournamentAddTeamController controller = loader.getController();
            controller.setTournamentName(tournamentName);
        }
    }

    /**
     * This method gives the user an option to use the ENTER key, rather than clicking the button.
     * @param ke Key to be pressed.
     * @throws IOException if an IO exception has occurred.
     */
    public void onEnter(KeyEvent ke) throws IOException {
        if(ke.getCode().equals(KeyCode.ENTER)){
            handleButtonNext();
        }
    }
}
