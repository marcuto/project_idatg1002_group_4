package no.idatg1002.TournamentApp;

import java.util.ArrayList;
import java.util.List;

/**
 * The tournament super class
 */
public abstract class Tournament {

    //List of matches
    private List<Match> matches;
    //List of teams in tournament
    private List<Team> teamsInTournament;
    //String variable to hold tournament name
    private String nameOfTournament;

    /**
     * Creates a new tournament
     * @param nameOfTournament name of tournament
     */
    protected Tournament(String nameOfTournament) {
        this.nameOfTournament = nameOfTournament;
        this.matches = new ArrayList<>();
        this.teamsInTournament = new ArrayList<>();
    }

    protected Tournament(){
        this.matches = new ArrayList<>();
        this.teamsInTournament = new ArrayList<>();
    }

    protected Tournament(List<Match> matches, List<Team> teamsInTournament, String nameOfTournament) {
        this.matches = matches;
        this.teamsInTournament = teamsInTournament;
        this.nameOfTournament = nameOfTournament;
    }

    /**
     * List of matches to be played in tournament
     * @return matches in the tournament
     */
    public List<Match> getMatches() {
        return matches;
    }

    /**
     * List of teams to compete in tournament
     * @return teams in the tournament
     */
    public List<Team> getTeamsInTournament() {
        return teamsInTournament;
    }

    /**
     * Return name of tournament
     * @return name of tournament
     */
    public String getNameOfTournament() {
        return nameOfTournament;
    }

    /**
     * Abstract method that generates method to be used in TournamentTable and TournamentKnockout.
     */
    public abstract void createMatches();

    /**
     * Abstract method that returns number of rounds in TournamentTable and TournamentKnockout.
     * @return number of rounds in tournament
     */
    public abstract int getNrOfRounds();
}
