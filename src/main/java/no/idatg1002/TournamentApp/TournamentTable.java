package no.idatg1002.TournamentApp;

import java.util.*;

/**
 * The TournamentTable class that extends the Tournament superclass.
 */
public class TournamentTable extends Tournament{

    /**
     * Basic constructor for the TournamentTable class.
     */
    public TournamentTable() {
        super("");
    }

    /**
     * Constructor for the TournamentTable class.
     * @param matches List of matches in the tournament.
     * @param teamsInTournament List of teams in the tournament.
     * @param nameOfTournament Name of the tournament.
     */
    public TournamentTable(List<Match> matches, List<Team> teamsInTournament, String nameOfTournament) {
        super(matches, teamsInTournament, nameOfTournament);
    }

    /**
     * Method that creates matches for the tournament.
     */
    @Override
    public void createMatches() {
        //if odd number of teams add a walkover "team"
        if (super.getTeamsInTournament().size() % 2 != 0){
            super.getTeamsInTournament().add(new Team("Walkover"));
        }

        List<Team> teamsNotFirst = new ArrayList<>();
        teamsNotFirst.addAll(super.getTeamsInTournament());
        teamsNotFirst.remove(0);

        for (int i = 0; i < this.getNrOfRounds(); i++) {
            int teamsNotFirstIdx = i % teamsNotFirst.size();

            super.getMatches().add(new Match(super.getTeamsInTournament().get(0), teamsNotFirst.get(teamsNotFirstIdx)));

            for (int j = 1; j < super.getTeamsInTournament().size()/2; j++) {
                int team1Idx = (i+j) % teamsNotFirst.size();
                int team2Idx = (i+teamsNotFirst.size()-j) % teamsNotFirst.size();

                super.getMatches().add(new Match(teamsNotFirst.get(team1Idx), teamsNotFirst.get(team2Idx)));
            }
        }
    }

    /**
     * Method that retrieves the number of rounds in the tournament.
     * @return Returns number of rounds in the tournament.
     */
    @Override
    public int getNrOfRounds() {
        return super.getTeamsInTournament().size()-1;
    }

    /**
     * Method that updates the position of all teams in the table.
     */
    public void updateRankings() {
        Collections.sort(super.getTeamsInTournament(), new Comparator<Team>() {
            @Override
            public int compare(Team o1, Team o2) {
                return o2.getGoalDifference() - o1.getGoalDifference();
            }
        });

        Collections.sort(super.getTeamsInTournament(), new Comparator<Team>() {
            @Override
            public int compare(Team o1, Team o2) {
                return o2.getPoints() - o1.getPoints();
            }
        });

        for (int i = 0; i < super.getTeamsInTournament().size(); i++) {
            Team t = super.getTeamsInTournament().get(i);
            t.setPosition(super.getTeamsInTournament().indexOf(t)+1);
        }

    }
}
