package no.idatg1002.TournamentApp;

import java.util.UUID;

/**
 * The team class with values that represent it.
 */
public class Team {
    //Variables with basic team information
    private String teamName;
    private String teamId;

    //Variables that will be assigned to a team in a table
    private int position;
    private int gamesPlayed;
    private int wins;
    private int draws;
    private int losses;
    private int goalDifference;
    private int points;

    /**
     * Constructor that creates a new team for the TournamentTable class
     * @param teamId Id of the team. Used if there are two teams of the same name.
     * @param teamName Name of the team.
     * @param position The position of the team in TournamentTable.
     * @param gamesPlayed The amount of games played of the team in the TournamentTable.
     * @param wins The amount of wins of the team in the TournamentTable.
     * @param draws The amount of draws of the team in the TournamentTable.
     * @param losses The amount of losses of the team in the TournamentTable.
     * @param goalDifference The goal difference of the team in the TournamentTable.
     * @param points The amount of points of team in the TournamentTable.
     */
    public Team(String teamId, String teamName, int position, int gamesPlayed, int wins, int draws, int losses, int goalDifference, int points) {
        this.teamName = teamName;
        this.teamId = teamId;
        this.position = position;
        this.gamesPlayed = gamesPlayed;
        this.wins = wins;
        this.draws = draws;
        this.losses = losses;
        this.goalDifference = goalDifference;
        this.points = points;
    }

    /**
     * Constructor that creates a new team
     * @param teamName takes team name as input
     */
    public Team(String teamName) {
        this.teamName = teamName;
        this.teamId = UUID.randomUUID().toString();
        this.points = 0;
    }

    /**
     * Constructor used to initialize the TournamentTable with base values
     * @param position Position of the team in the table
     * @param teamName Name of the team in the table.
     */
    public Team(int position, String teamName) {
        this.position = position;
        this.teamName = teamName;
        this.gamesPlayed = 0;
        this.wins = 0;
        this.draws = 0;
        this.losses = 0;
        this.goalDifference = 0;
        this.points = 0;
    }

    /**
     * Standard getters for Team variables
     * @return Returns Team variables
     */
    public String getTeamName() {
        return teamName;
    }

    public String getTeamId() {
        return teamId;
    }

    public int getPoints() {
        return points;
    }

    public int getPosition(){return this.position;}

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public int getWins() {
        return wins;
    }

    public int getDraws() {
        return draws;
    }

    public int getLosses() {
        return losses;
    }

    public int getGoalDifference() {
        return goalDifference;
    }

    /**
     * Standard setters for Team variables.
     * @param points New value of the team variable,
     */
    public void setPoints(int points) {
        this.points = points;
    }

    public void setPosition(int position){
        this.position = position;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public void setGoalDifference(int goalDifference) {
        this.goalDifference = goalDifference;
    }
}
