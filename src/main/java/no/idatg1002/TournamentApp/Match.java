package no.idatg1002.TournamentApp;

/**
 * The match class, creates an instance of a match
 */
public class Match {
    //Variable to hold team 1 in match
    private Team teamOne;
    //Variable to hold team 2 in match
    private Team teamTwo;
    //Variable to hold score
    private String score;
    //Variable to hold winner of match
    private Team winner;
    //Variable to hold loser of match
    private Team loser;
    //Variable to hold score difference of match
    private int scoreDifference;

    /**
     * Creates instance of match
     * @param teamOne team one to compete in match
     * @param teamTwo team two to compete in match
     */
    public Match(Team teamOne, Team teamTwo) {
        this.teamOne = teamOne;
        this.teamTwo = teamTwo;
    }

    public Match(Team teamOne, Team teamTwo, String score, Team winner) {
        this.teamOne = teamOne;
        this.teamTwo = teamTwo;
        this.score = score;
        this.winner = winner;
    }

    /**
     * Returns team one to compete in match
     * @return returns team one in match
     */
    public Team getTeamOne() {
        return teamOne;
    }

    /**
     * Returns team two to compete in match
     * @return returns team two in match
     */
    public Team getTeamTwo() {
        return teamTwo;
    }

    /**
     * Returns score result of match
     * @return returns score of match
     */
    public String getScore() { return score; }

    /**
     * Sets score of match
     * @param score of match as input
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     * Standard getter for scoreDifference field.
     * @return scoreDifference
     */
    public int getScoreDifference(){
        return scoreDifference;
    }

    /**
     * Returns winner of match
     * @return returns the winner of match
     */
    public Team getWinner() {
        return winner;
    }

    public Team getLoser(){
        return loser;
    }

    /**
     * Sets the winner of match
     */
    public void setWinner() {
        int result = getResult();
        if (result >= 1){
            this.winner = teamOne;
            this.loser = teamTwo;
        }
        else if (result <= -1){
            this.winner = teamTwo;
            this.loser = teamOne;
        }
        else {
            this.winner = null;
        }
    }

    /**
     * Method that retrieves the result for a match.
     * @return Returns 1, 0 or -1 depending on the retrieved values.
     */
    public int getResult(){
        String[] matchScore = this.score.split(":");
        scoreDifference = Math.abs(Integer.parseInt(matchScore[0]) - Integer.parseInt(matchScore[1]));
        return Integer.compare(Integer.parseInt(matchScore[0]), Integer.parseInt(matchScore[1]));
    }

    /**
     * Method that updates the values of all variables connected to the table format
     * depending on the result being a win, draw or loss for the team.
     */
    public void updateValues(){
        if (this.winner != null){
            //Updates the values for the winner
            this.winner.setPoints(this.winner.getPoints() + 3);
            this.winner.setWins(this.winner.getWins() + 1);
            this.winner.setGamesPlayed(this.winner.getGamesPlayed() + 1);
            this.winner.setGoalDifference(this.winner.getGoalDifference() + scoreDifference);

            //Updates the values of the loser
            this.loser.setLosses(this.loser.getLosses() + 1);
            this.loser.setGamesPlayed(this.loser.getGamesPlayed() + 1);
            this.loser.setGoalDifference(this.loser.getGoalDifference() - scoreDifference);
        }
        else{
            //Updates the values for both teams if there is a draw
            this.teamOne.setPoints(teamOne.getPoints() + 1);
            this.teamOne.setDraws(teamOne.getDraws() + 1);
            this.teamOne.setGamesPlayed(teamOne.getGamesPlayed() + 1);

            this.teamTwo.setPoints(teamTwo.getPoints() + 1);
            this.teamTwo.setDraws(teamTwo.getDraws() + 1);
            this.teamTwo.setGamesPlayed(teamTwo.getGamesPlayed() + 1);
        }
    }

    /**
     * toString method for displaying match contestants.
     * @return Returns a String of the two teams in the match.
     */
    @Override
    public String toString() {
        if (winner != null){
            return teamOne.getTeamName()+" v "+teamTwo.getTeamName() + ". Winner is: " + winner.getTeamName();
        }
        return teamOne.getTeamName()+" v "+teamTwo.getTeamName();
    }

    public String getMatchDisplay(){
        return this.toString();
    }

}
