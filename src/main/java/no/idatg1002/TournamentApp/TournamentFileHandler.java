package no.idatg1002.TournamentApp;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TournamentFileHandler {

    private final String folderName;
    private final String teamFile;
    private final String tournFile;

    /**
     * Constructor for TournamentFileHandler.
     */
    public TournamentFileHandler(){
        this.folderName = System.getenv("APPDATA") + File.separator + "TournamentAppFiles";
        this.teamFile = folderName + File.separator + "teams.csv";
        this.tournFile = folderName + File.separator + "tournament.csv";
        File dir = new File(folderName);
        if (!dir.exists()){
            dir.mkdir();
        }

    }

    /**
     * Create a list of teams from CSV file.
     * @return List of teams.
     */
    private List<Team> readTeamsCsv(){
        ArrayList<Team> teams = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(teamFile))) {
            String lineOfText;

            while ((lineOfText = reader.readLine()) != null) {
                String[] words = lineOfText.split(",");
                 teams.add(new Team(words[0],words[1],Integer.parseInt(words[2]),Integer.parseInt(words[3]),Integer.parseInt(words[4]),
                         Integer.parseInt(words[5]),Integer.parseInt(words[6]),Integer.parseInt(words[7]),Integer.parseInt(words[8])));

            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return teams;
    }
    /**
     * Create a tournament from a CSV file.
     * @return Tournament with list of teams, list of matches and name.
     */
    public Tournament readTournamentMatchCsv() {
        String name = "";
        String type = "";
        ArrayList<Match> matches = new ArrayList<>();
        List<Team> teams = readTeamsCsv();

        try (BufferedReader reader = new BufferedReader(new FileReader(tournFile))) {
            String lineOfText;

            if((lineOfText = reader.readLine()) != null){
                String[] words = lineOfText.split(",");
                name = words[0];
                type = words[1];
            }

            while ((lineOfText = reader.readLine()) != null) {
                String[] words = lineOfText.split(",");
                Team team1 = teams.stream().filter(team -> team.getTeamId().equals(words[0])).findAny().orElse(null);
                Team team2 = teams.stream().filter(team -> team.getTeamId().equals(words[1])).findAny().orElse(null);
                Team winner = teams.stream().filter(team -> team.getTeamId().equals(words[3])).findAny().orElse(null);
                matches.add(new Match(team1,team2,words[2],winner));

            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        if (type.equalsIgnoreCase("tournamentknockout")){
            return new TournamentKnockout(matches,teams,name);
        }
        return new TournamentTable(matches,teams,name);
    }

    /**
     * Write a CSV file from the tournament.
     */
    public void writeTournamentMatchCsv(List<Match> matches, String tournamentName, String tournamentType){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(tournFile))) {
            writer.write(tournamentName+","+tournamentType+"\n");
            for (Match m : matches) {
                if (m.getWinner() != null) {
                    writer.write(m.getTeamOne().getTeamId() + "," + m.getTeamTwo().getTeamId() + "," + m.getScore() + "," + m.getWinner().getTeamId() + "\n");
                } else {
                    writer.write(m.getTeamOne().getTeamId() + "," + m.getTeamTwo().getTeamId() + "," + m.getScore() + "," + "null" + "\n");
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Writes a CSV file for teams in tournament.
     * @param teams List of teams.
     */
    public void writeTeamsCsv(List<Team> teams) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(teamFile))) {
            for (Team t : teams) {
                writer.write(t.getTeamId() + "," + t.getTeamName() + "," + t.getPosition() + "," + t.getGamesPlayed() + "," + t.getWins() + "," +
                        t.getDraws() + "," + t.getLosses() + "," + t.getGoalDifference() + "," + t.getPoints() + "\n");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Clears teamFile and tourFile
     */
    public void clearAll(){

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(teamFile))) {
            writer.write("");
            writer.flush();
            }
         catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(tournFile))) {
            writer.write("");
            writer.flush();
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
