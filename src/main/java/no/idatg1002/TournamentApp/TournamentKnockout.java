package no.idatg1002.TournamentApp;

import java.util.ArrayList;
import java.util.List;

/**
 * The TournamentKnockout class that extends the Tournament super class.
 */
public class TournamentKnockout extends Tournament {

    /**
     * Basic constructor for TournamentKnockout.
     */
    public TournamentKnockout() {
        super("nameOfTournament");
    }

    /**
     * Constructor for TournamentKnockout.
     * @param matches List of matches in the tournament.
     * @param teamsInTournament List of teams in the tournament.
     * @param nameOfTournament Name of the tournament.
     */
    public TournamentKnockout(List<Match> matches, List<Team> teamsInTournament, String nameOfTournament) {
        super(matches, teamsInTournament, nameOfTournament);
    }

    /**
     * Method that creates matches for TournamentKnockout.
     */
    @Override
    public void createMatches() {
        if (isPowerOfTwo(super.getTeamsInTournament().size())) {
            if(super.getTeamsInTournament().size() == 1){
                return;
            }
            else{
                for (int i = 0; i < super.getTeamsInTournament().size(); i += 2) {
                    super.getMatches().add(new Match(super.getTeamsInTournament().get(i), super.getTeamsInTournament().get(i + 1)));
                }
            }
        }
    }

    /**
     * Check if number is to the power of two
     *
     * @param a Number to be checked.
     * @return Returns true or false.
     */
    public boolean isPowerOfTwo(int a) {
        return a != 0 && ((a & (a - 1)) == 0);
    }


    /**
     * Get the number of rounds for tournament.
     *
     * @return Returns the number of rounds in KnockoutTournament.
     */
    @Override
    public int getNrOfRounds() {
        int a = super.getTeamsInTournament().size();
        if (!isPowerOfTwo(a)) {
            return 0;
        }
        return (int) (Math.log(a) / Math.log(2));
    }

    /**
     * Method that retrieves the winner of each match and puts it in a list.
     * @return Returns a list of winners.
     */
    public List<Team> getWinners() {
        ArrayList<Team> winnerList = new ArrayList<>();
        super.getMatches().forEach(match -> winnerList.add(match.getWinner()));
        return winnerList;
    }

    /**
     * Method that removes all the losers from the list of teams.
     */
    public void removeLosers() {
        super.getTeamsInTournament().removeIf(team -> !getWinners().contains(team));
    }

    /**
     * Method that starts the next round of the tournament.
     */
    public void nextRound() {
        this.removeLosers();
        this.getMatches().clear();
        this.createMatches();
    }
}
