package no.idatg1002.TournamentApp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class TournamentTableTest {

    private TournamentTable tk;
    private Random r;

    @BeforeEach
    void createTeams(){
        this.r = new Random();
         this.tk = new TournamentTable();
        for (int i = 0; i < 8; i++) {
            tk.getTeamsInTournament().add(new Team("team"+i));
        }
    }

    @Test
    void createMatches() {
       tk.createMatches();
       //nr of matches in a round robin tournament is given by n/2*(n-1), where n is nr of teams
        int nrMatches = (tk.getTeamsInTournament().size()/2)*(tk.getTeamsInTournament().size()-1);
       assertEquals(nrMatches, tk.getMatches().size());
    }

    @Test
    void updateRankings() {
        tk.getTeamsInTournament().forEach(team -> team.setPoints(r.nextInt(10)));
        tk.updateRankings();
        Iterator<Team> it = tk.getTeamsInTournament().iterator();
        int score = 10000;
        while (it.hasNext()){
            Team t = it.next();
            if (t.getPoints()>score){
                fail();
            }
            score = t.getPoints();
        }

    }
}