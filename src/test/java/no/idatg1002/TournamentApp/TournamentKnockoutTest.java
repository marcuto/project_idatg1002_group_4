package no.idatg1002.TournamentApp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TournamentKnockoutTest {
    private TournamentKnockout tk;


    @BeforeEach
    void beforeEach() {
        tk = new TournamentKnockout();
        for (int i = 0; i < 8; i++) {
            tk.getTeamsInTournament().add(new Team("team"+i));
        }
    }

    @Test
    void createMatches() {
        tk.createMatches();
        int nrMatches = 0;
        while (tk.getTeamsInTournament().size()>1) {
            tk.getMatches().forEach(match -> match.setScore("1:0"));
            tk.getMatches().forEach(Match::setWinner);
            nrMatches+=tk.getMatches().size();
            tk.nextRound();

        }
        //nr of matches given in knockout style tournament is given by n-1 where n is the nr of teams
        assertEquals(7,nrMatches);
    }

    @Test
    void isPowerOfTwo() {
        assertEquals(true, tk.isPowerOfTwo(16));
        assertEquals(false, tk.isPowerOfTwo(10));
    }

    @Test
    void getNrOfRounds() {
        //nr of rounds in single elimination is given by 2^m=n where m is number of rounds and n is nr of teams
        assertEquals(3, tk.getNrOfRounds());
    }

    @Test
    void getWinners() {
        tk.createMatches();
        tk.getMatches().forEach(match -> match.setScore("1:0"));
        tk.getMatches().forEach(Match::setWinner);
        assertEquals(4, tk.getWinners().size());
    }

    @Test
    void removeLosers() {
        tk.createMatches();
        tk.getMatches().forEach(match -> match.setScore("1:0"));
        tk.getMatches().forEach(match -> match.setWinner());
        tk.removeLosers();
        assertEquals(4, tk.getTeamsInTournament().size());
    }

    @Test
    void nextRound() {
        tk.createMatches();
        tk.getMatches().forEach(match -> match.setScore("1:0"));
        tk.getMatches().forEach(Match::setWinner);
        assertEquals(8, tk.getTeamsInTournament().size());
        tk.nextRound();
        assertEquals(4, tk.getTeamsInTournament().size());
    }


}