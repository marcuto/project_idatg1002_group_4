package no.idatg1002.TournamentApp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;



class MatchTest {

    Team team1 = new Team("Chelsea");
    Team team2 = new Team("Arsenal");

    Match match = new Match(team1, team2);

    @BeforeEach
    void setUp() {
    }
    @Test
    void setWinner(){
        match.setScore("3:1");
        match.setWinner();
        try{
            assertEquals(team1, match.getWinner());
            assertEquals(team2, match.getLoser());
        } catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }

        match.setScore("2:5");
        match.setWinner();
        try{
            assertEquals(team2, match.getWinner());
            assertEquals(team1, match.getLoser());
        } catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }

        match.setScore("2:2");
        match.setWinner();
        try{
            assertEquals(null, match.getWinner());
        } catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }
    }
    @Test
    void getResult(){
        match.setScore("3:1");
        try{
            assertEquals(1, match.getResult());
            assertEquals(2, match.getScoreDifference());
        }catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }

        match.setScore("2:5");
        try{
            assertEquals(-1, match.getResult());
            assertEquals(3, match.getScoreDifference());
        }catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }

        match.setScore("2:2");
        try{
            assertEquals(0, match.getResult());
            assertEquals(0, match.getScoreDifference());
        } catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }
    }
    @Test
    void updateValues() {
        match.setScore("3:1");
        match.setWinner();
        match.updateValues();
        try{
            assertEquals(3, team1.getPoints());
            assertEquals(1, team1.getWins());
            assertEquals(0, team1.getDraws());
            assertEquals(0, team1.getLosses());
            assertEquals(1, team1.getGamesPlayed());
            assertEquals(2, team1.getGoalDifference());

            assertEquals(0, team2.getPoints());
            assertEquals(0, team2.getWins());
            assertEquals(0, team2.getDraws());
            assertEquals(1, team2.getLosses());
            assertEquals(1, team2.getGamesPlayed());
            assertEquals(-2, team2.getGoalDifference());
        }catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }

        team1.setGoalDifference(0);
        team1.setGamesPlayed(0);
        team1.setLosses(0);
        team1.setDraws(0);
        team1.setWins(0);
        team1.setPoints(0);

        team2.setGoalDifference(0);
        team2.setGamesPlayed(0);
        team2.setLosses(0);
        team2.setDraws(0);
        team2.setWins(0);
        team2.setPoints(0);

        match.setScore("2:5");
        match.setWinner();
        match.updateValues();
        try{
            assertEquals(3, team2.getPoints());
            assertEquals(1, team2.getWins());
            assertEquals(0, team2.getDraws());
            assertEquals(0, team2.getLosses());
            assertEquals(1, team2.getGamesPlayed());
            assertEquals(3, team2.getGoalDifference());

            assertEquals(0, team1.getPoints());
            assertEquals(0, team1.getWins());
            assertEquals(0, team1.getDraws());
            assertEquals(1, team1.getLosses());
            assertEquals(1, team1.getGamesPlayed());
            assertEquals(-3, team1.getGoalDifference());
        }catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }

        team1.setGoalDifference(0);
        team1.setGamesPlayed(0);
        team1.setLosses(0);
        team1.setDraws(0);
        team1.setWins(0);
        team1.setPoints(0);

        team2.setGoalDifference(0);
        team2.setGamesPlayed(0);
        team2.setLosses(0);
        team2.setDraws(0);
        team2.setWins(0);
        team2.setPoints(0);

        match.setScore("2:2");
        match.setWinner();
        match.updateValues();
        try{
            assertEquals(1, team1.getPoints());
            assertEquals(0, team1.getWins());
            assertEquals(1, team1.getDraws());
            assertEquals(0, team1.getLosses());
            assertEquals(1, team1.getGamesPlayed());
            assertEquals(0, team1.getGoalDifference());

            assertEquals(1,team2.getPoints());
            assertEquals(0, team2.getWins());
            assertEquals(1, team2.getDraws());
            assertEquals(0, team2.getLosses());
            assertEquals(1, team2.getGamesPlayed());
            assertEquals(0, team2.getGoalDifference());
        }catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }
    }
}